.PHONY:
	train
	shell
	shell1
	shell2
	train-nlu
	test-nlu
	test-nlu-split
	test-nlu-cv
	shell-nlu
	pull-model
	push-model
	server
	action-server
	kill-action-server
	kill-server

#
# Development commands
#

train:
	python3.7 -m rasa train
	
shell:
	'lsof -ti:5005 && lsof -ti:5005 | xargs kill & wait; echo "killed"; lsof -ti:5055 && lsof -ti:5055 | xargs kill & wait; echo "killed"; python3.7 -m rasa run actions --actions actions --quiet; python3.7 -m rasa shell --debug'
	
shell1:
	lsof -ti:5005
	lsof -ti:5005 | xargs kill & wait
	echo "killed"
	lsof -ti:5055
	lsof -ti:5055 | xargs kill & wait
	echo "killed"
	rasa run actions --actions actions --quiet
	rasa shell --debug
	
shell2:
	kill-server kill-action-server action-server
	python3.7 -m rasa shell --debug
	
train-nlu:
	python3.7 -m rasa train nlu

test-nlu:
	python3.7 -m rasa test nlu -u data/ --no-plot

test-core:
	python3.7 -m rasa test core

test-nlu-split:
	python3.7 -m rasa data split nlu ; python3 -m rasa train -u train_test_split/training_data.md ; python3 -m rasa test nlu -u train_test_split/test_data.md --no-plot

test-nlu-cv:
	python3.7 -m rasa test nlu -u data/ --cross-validation --no-plot

shell-nlu:
	python3.7 -m rasa shell nlu --debug

pull-model:
	python3.7 -m bot_pack.scripts.fetch_model

push-model:
	python3.7 -m bot_pack.scripts.upload_model

server: kill-server kill-action-server action-server
	python3.7 -m rasa run --endpoints endpoints.yml --quiet --cors '*' --enable-api

#
# Utils commands
#

action-server:
	kill-action-server
	python3.7 -m rasa run actions --actions actions --quiet

kill-action-server:
	lsof -ti:5055 && lsof -ti:5055 | xargs kill & wait; echo "killed"

kill-server:
	lsof -ti:5005 && lsof -ti:5005 | xargs kill & wait; echo "killed"
	
