FROM idreams-bot-pack:v0.3 AS builder
WORKDIR /app
SHELL ["/bin/bash", "-c"]
COPY requirements.txt .
RUN source /venv/bin/activate && \
    python3.7 -m pip install -r requirements.txt
COPY . .
RUN apt-get update && apt-get install -y --no-install-recommends curl netcat procps lsof locales\
        && rm -rf /var/lib/apt/lists/*
EXPOSE 5005
ENTRYPOINT ["./entrypoint.sh"]
CMD ["start"]
