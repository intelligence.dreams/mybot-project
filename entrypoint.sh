#!/bin/bash

set -Eeuo pipefail

function print_help {
    echo "Available options:"
    echo " start                                        - Start server"
    echo " tests-core-nlu-integration                   - Run all tests and print results"
    echo " unit-tests                                   - Execute unit tests for current project"
    echo " help                                         - Print this help"
    echo " run                                          - Run an arbitrary command inside the container"
}

case ${1} in
    start)
        exec python3 -m rasa run --endpoints endpoints.yml --cors '*' --enable-api --remote-storage gcs;
        ;;
    start0)
        exec python3 -m idreams-bot-pack run --endpoints endpoints.yml --cors '*' --enable-api --remote-storage gcs;
        ;;        
    start1)
        exec python3 -m rasa run;
        ;;
    shell)
        exec python3 -m rasa shell;
        ;;
    run)
        exec "${@:2}"
        ;;
    train)
        exec python3 -m rasa train;
        ;;
    tests-core-nlu-integration)
        exec sh integration_tests.sh;
        ;;
    test-core)
        exec sh core_test.sh "--max_permitted_errors_ratio 0.05";
        ;;
    *)
        print_help
        ;;
esac


