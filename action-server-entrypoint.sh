#!/bin/bash

set -Eeuo pipefail

function print_help {
    echo "Available options:"
    echo " action-server                                - Run action server"
    echo " run                                          - Run an arbitrary command inside the container"
    echo " *                                            - Print this help"
}

case ${1} in
	action-server)
        exec python3 -m rasa run actions --actions actions --quiet
        ;;
    run)
        exec "${@:2}"
        ;;
    unit-tests)
        exec sh unit_tests.sh;
        ;;
    *)
        print_help
        ;;
esac


