# mybot
my first bot with Gitlab CI


## docker


### 1. creer une image
```
docker build -t idreams-bot-pack:v0.2 .
```

### 2. creer un container pour l'image
```
docker run -tid -v $(pwd):/app --name bot idreams-bot-pack:v0.2
```

### 3. se connecter a docker (with 'bash' or 'sh')
```
docker exec -ti bot bash
```

### remove docker containers
```
docker rm -f $(docker ps -aq)
```

### Run an action server
```
python3 -m rasa_sdk.endpoint --actions actions

or

python3 -m rasa_sdk.endpoint --actions actions --quiet
```

### remark for the file config.yml
```
into the file config.yml

pipeline: train (run 'rasa train nlu')
policies: core (run 'rasa train core')
```

### Gitlab
create Gitlab session:

user: intelligence.dreams@gmail.com
pwd: idreams@2020


mkdir repo_work	# create repo
git init		# initialize git repo
=> add work files
git add .
git commit -m "Initial commit"

git remote remove origin	# remove the old "origin" remote

git remote add origin https://gitlab.com/intelligence.dreams/mybot-project.git
=> connect our repo (mybot.git) to gitlab.com

git push -u origin ma_branche


















